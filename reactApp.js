class Team extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      shots: 0,
      score: 0
    }

    this.shotSound = new Audio('./assets/audio/caughtball.wav')
    this.scoreSound = new Audio('./assets/audio/Swish.wav')
  }

  shotHandler = () => {
    let score = this.state.score
    this.shotSound.play()


    if (Math.random() > 0.5) {
      score += 1

      setTimeout(() => {
        this.scoreSound.play()

      }, 100)


    }
    this.setState((state, props) => ({
      shots: this.state.shots + 1,
      score
    }))
  }

  render() {
    let shotPercentageDiv

    if (this.state.shots) {
      const shotPercentage = Math.round((this.state.score / this.state.shots) * 100)
      shotPercentageDiv = (
        <div>
          <strong>Shooting %: {shotPercentage}</strong>
        </div>
      )
    }


    return (
      <div className="Team">
        <h2>{this.props.name}</h2>

        <div className="identity">
          <img src={this.props.logo} alt={this.props.name}
          />
        </div>

        <div>
          <strong>Shots:</strong> {this.state.shots}
        </div>

        <div>
          <strong>Score:</strong> {this.state.score}
        </div>

        {shotPercentageDiv}


        <button onClick={this.shotHandler}>Shoot!</button>
      </div>
    )
  }
}

function Game(props) {
  return (
    <div className="Game">
      <h1>Welcome to {props.venue}</h1>
      <div className="stats">
        <Team
          name={props.visitingTeam.name}
          logo={props.visitingTeam.logoSrc}
        />

        <div className="versus">
          <h1>VS</h1>
        </div>

        <Team
          name={props.homeTeam.name}
          logo={props.homeTeam.logoSrc} />
        />
      </div>
    </div>
  )
}


function App(props) {
  const eagles = {
    name: "Zionsville Eagles",
    logoSrc: "./assets/images/eagles.png",
  }

  const tigers = {
    name: "Lebanon Tigers",
    logoSrc: "./assets/images/858-min 2.png"
  }

  const droid = {
    name: "Desert County Droids",
    logoSrc: "./assets/images/droids.png"
  }

  const venom = {
    name: "Oakville Venom",
    logoSrc: "./assets/images/venom.png"
  }

  return (
    <div className="App">
      <Game
        venue="Zionsville Gym"
        homeTeam={eagles}
        visitingTeam={tigers}
      />
      <Game
        venue="Droid Arena"
        homeTeam={droid}
        visitingTeam={venom}
      />

    </div>
  )
}
//Render the application
ReactDOM.render(<App />, document.getElementById("root"));
